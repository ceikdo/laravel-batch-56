<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php

    echo "<h3> Soal No 1 Greetings </h3>";
    /* 
Soal No 1
Greetings
Buatlah sebuah function greetings() yang menerima satu parameter berupa string. 

contoh: greetings("abduh");
Output: "Halo Abduh, Selamat Datang di Sanbercode!"
*/

    // Code function di sini
    function greetings($name)
    {
        $message = "Halo $name, Selamat Datang di Sanbercode!" . "<br>";
        echo $message;
        return $message;
    }


    // Hapus komentar untuk menjalankan code!
    greetings("Bagas");
    greetings("Wahyu");
    greetings("Jabal Abdul Salam");

    echo "<br>";

    echo "<h3>Soal No 2 Reverse String</h3>";
    /* 
Soal No 2
Reverse String
Buatlah sebuah function reverseString() untuk mengubah string berikut menjadi kebalikannya menggunakan function dan looping (for/while/do while).
Function reverseString menerima satu parameter berupa string.
NB: DILARANG menggunakan built-in function PHP sepert strrev(), HANYA gunakan LOOPING!

reverseString("abdul");
Output: ludba

*/

    // Code function di sini 
    function reverseString($string)
    {
        $length = strlen($string);

        $reverseStr = " ";

        for ($i = $length - 1; $i >= 0; $i--) {
            $reverseStr .= $string[$i];
        }

        echo $reverseStr . "<br>";
        return $reverseStr;
    }



    reverseString("Jabal Abdul Salam");
    reverseString("Sanbercode");
    reverseString("We Are Sanbers Developers");
    echo "<br>";

    echo "<h3>Soal No 3 Palindrome </h3>";
    /* 
Soal No 3 
Palindrome
Buatlah sebuah function yang menerima parameter berupa string yang mengecek apakah string tersebut sebuah palindrome atau bukan. 
Palindrome adalah sebuah kata atau kalimat yang jika dibalik akan memberikan kata yang sama contohnya: katak, civic.
Jika string tersebut palindrome maka akan mengembalikan nilai true, sedangkan jika bukan palindrome akan mengembalikan false.
NB: 
Contoh: 
palindrome("katak") => output : "true"
palindrome("jambu") => output : "false"
NB: DILARANG menggunakan built-in function PHP seperti strrev() dll. Gunakan looping seperti biasa atau gunakan function reverseString dari jawaban no.2!

*/


    // Code function di sini
    function palindrome($input)
    {
        $input = strtolower(str_replace(' ', ' ', $input));

        $length = strlen($input);

        for ($i = 0; $i < $length / 2; $i++) {
            if ($input[$i] !== $input[$length - $i - 1]) {
                return false;
            }
        }
        return true;
    }

    // Hapus komentar di bawah ini untuk jalankan code
    echo palindrome("civic") ? "true" : "false"; // true
    echo "<br>";
    echo palindrome("nababan") ? "true" : "false"; // true
    echo "<br>";
    echo palindrome("jambaban") ? "true" : "false"; // false
    echo "<br>";
    echo palindrome("racecar") ? "true" : "false"; // true


    echo "<h3>Soal No 4 Tentukan Nilai </h3>";
    /*
Soal 4
buatlah sebuah function bernama tentukan_nilai . Di dalam function tentukan_nilai yang menerima parameter 
berupa integer. dengan ketentuan jika paramater integer lebih besar dari sama dengan 85 dan lebih kecil sama dengan 100 maka akan mereturn String “Sangat Baik” 
Selain itu jika parameter integer lebih besar sama dengan 70 dan lebih kecil dari 85 maka akan mereturn string “Baik” selain itu jika parameter number lebih besar 
sama dengan 60 dan lebih kecil dari 70 maka akan mereturn string “Cukup” selain itu maka akan mereturn string “Kurang”
*/

    // Code function di sini
    function tentukan_nilai($input)
    {
        if ($input >= 85 && $input <= 100) {
            echo "Sangat Baik" . "<br>";
        } elseif ($input >= 70 && $input < 85) {
            echo "Baik" . "<br>";
        } elseif ($input >= 60 && $input < 70) {
            echo "Cukup" . "<br>";
        } elseif ($input < 60) {
            echo "Kurang";
        } else {
            echo "Input tidak sesuai";
        }
    }

    // Hapus komentar di bawah ini untuk jalankan code
    tentukan_nilai(98); //Sangat Baik
    tentukan_nilai(76); //Baik
    tentukan_nilai(67); //Cukup
    tentukan_nilai(43); //Kurang


    ?>

</body>

</html>